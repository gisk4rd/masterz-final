import AlgoManager from "@/data/db/AlgoManager";
import React from "react";
import { Button } from "react-bootstrap";
import NavItem from "./NavItem";
import { useState } from 'react';

const ConnectButton = ({ }) => {

  const [requestedShares, setRequestedShared] = useState(0)
  const [transactionStatus, setTransactionStatus] = useState(0)
  // 0 : idle | 1 : payment process | 2 : payment done | -1 : error


  const getStatus = () => {
    switch (transactionStatus) {

      case 1:
        return "yellow"
      case 2:
        return "done"
      case -1:
        return "red"

      default:
      case 0:
        return ""
    }
  }

  const getName = () => {
    switch (transactionStatus) {

      case 1:
        return "Connecting..."
      case 2:
        return AlgoManager.getAlgoAddress() ?? "-"
      case -1:
        return "Connecting Error"

      default:
      case 0:
        return "Connect Wallet"
    }
  }


  const onConnectPressed = async () => {
    setTransactionStatus(1)
    try {
      const algo = await AlgoManager.connectToMyAlgo()
      setTransactionStatus(value ? 2 : -1)
    } catch (e) { setTransactionStatus(-1) }
    setTimeout(() => setTransactionStatus(0), 2000)
  }

  return (
    <Button onClick={onConnectPressed} className={`wallet-btn ${getStatus()}`} >{getName()}</Button>
  );

};

export default ConnectButton;
