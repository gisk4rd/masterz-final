import headerData from "@/data/headerData";
import useScroll from "@/hooks/useScroll";
import React from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import MainHeaderItem from "./MainHeaderItem";
import Social from "./Social";

const { logo, navItems, phone, icon, email, address, socials } = headerData;

const Header = ({ className = "" }) => {
  const { scrollTop  } = useScroll(160);
  
  return (
    <header className={`header-area ${className}`}>
      <div className={`main-header${scrollTop ? " sticky" : ""}`}>
        <Container>
          <MainHeaderItem
            logo={logo}
            navItems={navItems}
            icon={icon}
          />
        </Container>
      </div>
    </header>
  );
};

export default Header;
