import React from "react";
import { Button } from "react-bootstrap";
import ConnectButton from "./ConnectButton";
import NavItem from "./NavItem";

const HeaderMenu = ({ navItems = [] }) => {
  return (
    <div className="header-menu d-none d-lg-block">
      <ul>
        {
          navItems.map((navItem) => (
            <NavItem key={navItem.id} navItem={navItem} />
          ))
        }
        <ConnectButton />
      </ul>
    </div>
  );
};

export default HeaderMenu;
