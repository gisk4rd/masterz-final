import bg from "@/images/page-title-bg.jpg";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Link from "./Link";

const PageTitle = ({ title = "", page = "", parent = "" }) => {
  return (
    <section
      className="page-title-area bg_cover"
      style={{ backgroundColor: "#106a39" }}
    >
      <Container>
        <Row>
          <Col lg={12}>
            <div className="page-title-content">
              <h3 className="title">{title}</h3>
              <nav aria-label="breadcrumb">
              </nav>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default PageTitle;
