import { days_between } from "@/data/db/DBManager";
import { projectDetailsArea } from "@/data/projectsArea";
import React from "react";
import { Col, Container, Image, Row } from "react-bootstrap";
import ProjectInvestButton from "./ProjectInvestButton";

const {
  thumb,
  flag,
  tagline,
  country,
  title,
  pledged,
  backers,
  daysLeft,
  raised,
  goal,
  socials,
} = projectDetailsArea;

// @khadim
const ProjectDetailsArea = ({ campaign }) => {

  console.log(campaign)
  return (
    <section className="project-details-area pt-120 pb-190">
      <Container>
        <Row>
          <Col lg={7}>
            <div className="project-details-thumb">
              <Image src={campaign?.image ?? ""} alt="" />
              <div className="icon">
                <i className="fa fa-heart"></i>
              </div>
            </div>
          </Col>
          <Col lg={5}>
            <div className="project-details-content">
              <div className="details-btn">
                <span>{tagline}</span>
            
              </div>
              <h3 className="title">{campaign?.title}</h3>
              <div className="project-details-item">
                <div className="item text-center">
                  <h5 className="title">{campaign?.raised}</h5>
                  <span> Algo raised</span>
                </div>
                <div className="item text-center">
                  <h5 className="title">{(campaign?.goal - campaign?.raised)/ campaign?.sharePrice}</h5>
                  <span>Remaining NFTs</span>
                </div>
                <div className="item text-center">
                  <h5 className="title">{days_between(campaign?.date)}</h5>
                  <span>Days Left</span>
                </div>
              </div>
              <div className="projects-range">
                <div className="projects-range-content">
                  <ul>
                    <li>Raised:</li>
                    <li>{campaign?.raised * 100 / campaign?.goal}%</li>
                  </ul>
                  <div className="range"></div>
                </div>
              </div>
              <div className="projects-goal">
                <span>
                  Goal: <span>{campaign?.goal} ALGO</span>
                </span>
              </div>
              <ProjectInvestButton campaign={campaign} />

              {/*<div className="project-share d-flex align-items-center">
                <span>Share this Project</span>
                <ul>
                  {socials.map(({ id, icon, href }) => (
                    <li key={id}>
                      <a href={href}>
                        <i className={icon}></i>
                      </a>
                    </li>
                  ))}
                </ul>
                  </div>*/}
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default ProjectDetailsArea;
