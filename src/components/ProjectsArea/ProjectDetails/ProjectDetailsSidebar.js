import AlgoManager from "@/data/db/AlgoManager";
import { days_between } from "@/data/db/DBManager";
import { projectDetailsSidebar } from "@/data/projectsArea";
import React, { useState } from "react";
import { Col, Image } from "react-bootstrap";
import ProjectInvestButton from "./ProjectInvestButton";

const { info, perks } = projectDetailsSidebar;

const ProjectDetailsPark = ({ campaign = {} }) => {


  return (
    <div
      className={`project-details-park mt-30 box`}
    >
      <h4 className="title">Buy a Shares</h4>
      {campaign?.image && <Image src={campaign?.image} alt="" />}
      <span>({campaign?.raised / campaign?.sharePrice}/{campaign?.goal / campaign?.sharePrice} raised )</span>
      <p>
        NFTs {campaign?.goal / campaign?.sharePrice} ({campaign?.raised * 100 / campaign?.goal}% OFF)
      </p>
      <ul>
        <li>Expiring date</li>
        <li>
          <span>{days_between(campaign?.date)} days left</span>
        </li>

      </ul>

      <ProjectInvestButton campaign={campaign} />

    </div>
  );
};

const ProjectDetailsSidebar = ({ campaign }) => {
  return (
    <div className="project-details-sidebar">
      {/* <div className="project-details-info mt-70 box">
        <div className="info">
          <Image src={info.image.src} alt="" />
          <h5 className="title">{info.name}</h5>
          <span>{info.backed} backed</span>
        </div>
        <p>{info.text}</p>
  </div>*/}
      <ProjectDetailsPark key={campaign?.id ?? ""} campaign={campaign} />

    </div>
  );
};

export default ProjectDetailsSidebar;
