import AlgoManager from "@/data/db/AlgoManager";
import { days_between } from "@/data/db/DBManager";
import { projectDetailsSidebar } from "@/data/projectsArea";
import React, { useState } from "react";
import { Col, Image } from "react-bootstrap";

const { info, perks } = projectDetailsSidebar;

const ProjectInvestButton = ({ campaign = {} }) => {
  const [requestedShares, setRequestedShared] = useState(0)
  const [transactionStatus, setTransactionStatus] = useState(0)
  // 0 : idle | 1 : payment process | 2 : payment done | -1 : error

  const requestShares = async () => {
    setTransactionStatus(1)
    try {
      const value = await AlgoManager.requestShares(requestedShares, campaign)
      setTransactionStatus(value ? 2 : -1)
    } catch (e) { }
    setTimeout(() => setTransactionStatus(0), 2000)
  }

  const getStatus = () => {
    switch (transactionStatus) {

      case 1:
        return "yellow"
      case 2:
        return "done"
      case -1:
        return "red"

      default:
      case 0:
        return ""
    }
  }

  const getName = () => {
    switch (transactionStatus) {

      case 1:
        return "Processing..."
      case 2:
        return "Payment Done"
      case -1:
        return "Payment error"

      default:
      case 0:
        return "Invest Now"
    }
  }

  return (
    <div className="shares">
      <div className="input-shares">
        <input type="number" placeholder="N° Share" name="shares" onChange={(e) => setRequestedShared(e.target.value)} />
      </div>

      <a className={`main-btn ${getStatus()}`} onClick={requestShares} >
        {getName()}
      </a>

    </div>
  );
};

export default ProjectInvestButton;
