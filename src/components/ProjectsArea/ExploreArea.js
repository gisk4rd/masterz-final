import { projectsArea } from "@/data/projectsArea";
import React, { useEffect, useState } from "react";
import DBManager, { getAllCampaigns } from "@/data/db/DBManager";

import { Col, Container, Row } from "react-bootstrap";
import SingleProject from "./SingleProject";

const { projects } = projectsArea;

const ExploreArea = () => {
  const [dataset, setDataset] = useState([])

  const getAllData = async () => {
    const data = await getAllCampaigns()
    console.log(data)
    setDataset(data)

  }

  useEffect(() => {
    getAllData()
  }, [])




  return (
    <section className="explore-area pt-90 pb-120">
      <Container>
        <Row className="justify-content-center">
          {dataset.map((project) => (
            <Col lg={4} md={6} sm={7} key={project.id}>
              <SingleProject project={project} />
            </Col>
          ))}
        </Row>
      </Container>
    </section>
  );
};

export default ExploreArea;
