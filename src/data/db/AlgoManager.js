
import MyAlgoConnect from '@randlabs/myalgo-connect';
import algosdk from 'algosdk';

/* eslint import/no-webpack-loader-syntax: off */
import algogoProgram from "!!raw-loader!../contracts/algogo.teal";

class AlgoManager {



    //SMART CONTRACT DEPLOYMENT
    // declare application state storage (immutable)
    localInts = 4;
    localBytes = 0;
    globalInts = 3; //# 4 for setup + 20 for choices. Use a larger number for more choices.
    globalBytes = 0;


    // get accounts from mnemonic
    creatorMnemonic = "scan wheel heavy boy feature mind achieve crew comfort gauge valve crew assume doll pyramid insane toe tiger shed prevent color gown oil able inmate"
    userMnemonic = "hotel hole fox quit trend manage universe name sketch maximum toast normal develop favorite actual bean extra husband casual acquire seminar float moment ability nose"
    creatorAccount = null
    userAccout = null
    creatorSecret = null
    creatorAddress = null
    sender = null

    //Generate Account
    accountAlgo = null
    secrekey = null
    mnemonic = null

    algoConfig = {
        algodToken: "gDbKJb4mp07eIqPgOzLOt5jDw5KjlK4L78Xz9SNi",
        baseServer: "https://node.testnet.algoexplorerapi.io", //'https://testnet-algorand.api.purestake.io/ps2/',
        port: "443",
        headers: { "X-API-Key": "gDbKJb4mp07eIqPgOzLOt5jDw5KjlK4L78Xz9SNi" },
        indexerToken: "",
        indexerServer: "https://algoindexer.testnet.algoexplorerapi.io",
        indexerPort: "",
    }

    indexerClient = null
    client = null
    myAlgoWallet = null
    crowdFoundigContract = null
    account = null

    /** Inizializzazione algo account */

    initAccount() {
        this.creatorAccount = algosdk.mnemonicToSecretKey(this.creatorMnemonic)
        this.userAccout = algosdk.mnemonicToSecretKey(this.userMnemonic)
        this.creatorSecret = this.creatorAccount.sk
        this.creatorAddress = this.creatorAccount.addr
        this.sender = this.userAccout.addr
        //Generate Account
        this.accountAlgo = algosdk.generateAccount()
        this.secrekey = this.accountAlgo.sk
        this.mnemonic = algosdk.secretKeyToMnemonic(this.secrekey)
        console.log("mnemonic " + this.mnemonic)
        console.log("address " + this.accountAlgo.addr)
    }

    /** Inizializzazione algo sdk */
    async initialize() {
        // this.initAccount()
        this.client =
            new algosdk.Algodv2(this.algoConfig.algodToken,
                this.algoConfig.baseServer,
                this.algoConfig.port,
                this.algoConfig.headers)
        console.log(this.client)

        this.indexerClient = new algosdk.Indexer(this.algoConfig.indexerToken, this.algoConfig.indexerServer, this.algoConfig.indexerPort);
        console.log(this.indexerClient)

        await this.compileProgram(client)

        // 
        let status = await client.status().do()
        console.log(status)

        //this.crowdFoundigContract = fs.readFileSync('../contract/algogo.py', 'utf8')
    }


    requestShares = (shares, campaign, ) => {
        return new Promise(async (resolve, reject) => {
            try {
                !this.myAlgoWallet && await this.connectToMyAlgo()
                // decentralized
                // const price = campaign.sharePrice
                // const campaigns = this.buyShare(campaign,share,price)
                // resolve(campaigns)
                resolve(true)
            } catch (error) {
                resolve(false)
            }
            //setTimeout(() => resolve(true), 2000)
        })
    }

    getAlgoAddress() {
        return this.account.map(acc => acc.address)
    }


    /** Richiesta di connessione al wallet MyAlgo */
    connectToMyAlgo() {
        return new Promise(async (resolve, reject) => {
            if (!this.myAlgoWallet)
                this.myAlgoWallet = new MyAlgoConnect();
            try {
                this.account = await this.myAlgoWallet.connect();
                const addresses = this.account.map(acc => acc.address);
                this.initialize()
                resolve(this.account)
            } catch (err) {
                console.error(err);
                reject(err)
            }
        })
    }

    // Read Teal File
    algogoProgram = ''

    compileProgram = async (client) => {
        try {
            this.algogoProgram = fs.readFileSync('../contract/algogo.teal', 'utf8')
            console.log(this.algogoProgram)
        } catch (err) {
            console.error(err)
        }
    }


    // Compile Program
    compileProgram = async (client, programSource) => {
        let encoder = new TextEncoder();
        let programBytes = encoder.encode(programSource);
        let compileResponse = await client.compile(programBytes).do();
        let compiledBytes = new Uint8Array(Buffer.from(compileResponse.result, "base64"));
        // console.log(compileResponse)
        return compiledBytes;
    }

    // Rounds
    waitForRound = async (round) => {
        let last_round = await this.client.status().do()
        let lastRound = last_round['last-round']
        console.log("Waiting for round " + lastRound)
        while (lastRound < round) {
            lastRound += 1
            const block = await this.client.statusAfterBlock(lastRound).do()
            console.log("Round " + block['last-round'])
        }
    }

    // convert 64 bit integer i to byte string
    intToBytes = (integer) => {
        return integer.toString()
    }



    createCampaign = async ({
        campaignName,
        campaignSymbol,
        shareAccount,
        totalShares,
        pricePerShare,
        url,
        metadata
    }) => {
        console.log("Adding campaign...")

        let params = await this.client.getTransactionParams().do();
        params.fee = algosdk.ALGORAND_MIN_TX_FEE;
        params.flatFee = true;

        // Compile programs
        const compiledAlgogoProgram = await compileProgram(algogoProgram)
        //const compiledClearProgram = await compileProgram(clearProgram)

        // Build note to identify transaction later and required app args as Uint8Arrays
        let note = new TextEncoder().encode(""/*marketplaceNote*/);

        let campaignNameArg = new TextEncoder().encode(campaignName)
        let campaignSymbolArg = TextEncoder().encode(campaignSymbol)
        let shareAccountArg = new TextEncoder().encode(shareAccount)
        let totalSharesArg = algosdk.encodeUint64(totalShares);
        let pricePerShareArg = algosdk.encodeUint64(pricePerShare);


        let appArgs = [campaignNameArg,
            campaignSymbolArg,
            shareAccountArg,
            totalSharesArg,
            pricePerShareArg,
            urlArg,
            metadata]

        // Create ApplicationCreateTxn
        let txn = algosdk.makeApplicationCreateTxnFromObject({
            from: this.getAlgoAddress(),
            suggestedParams: params,
            onComplete: algosdk.OnApplicationComplete.NoOpOC,
            approvalProgram: compiledAlgogoProgram,
            // clearProgram: compiledClearProgram,
            numLocalInts: this.localInts,
            numLocalByteSlices: this.localBytes,
            numGlobalInts: this.globalInts,
            numGlobalByteSlices: this.globalBytes,
            note: note,
            appArgs: appArgs
        });

        // Get transaction ID
        let txId = txn.txID().toString();

        // Sign & submit the transaction
        let signedTxn = await this.myAlgoConnect.signTransaction(txn.toByte());
        console.log("Signed transaction with txID: %s", txId);
        await this.client.sendRawTransaction(signedTxn.blob).do();

        // Wait for transaction to be confirmed
        let confirmedTxn = await algosdk.waitForConfirmation(this.client, txId, 4);

        // Get the completed Transaction
        console.log("Transaction " + txId + " confirmed in round " + confirmedTxn["confirmed-round"]);

        // Get created application id and notify about completion
        let transactionResponse = await this.client.pendingTransactionInformation(txId).do();
        let appId = transactionResponse['application-index'];
        console.log("Created new app-id: ", appId);
        return appId;
    }



    buyShare = async ({
        campaign,
        shares,
        price
    }) => {
        console.log("Buying shares...");

        let appIndex = "115082653"
        let owner = "BSWFRVFN5EXSDND7KD5ZUC2QGHMJKW3NLXVEDNQX25M56CLUM4TGD6TFZ4"
        let params = await this.client.getTransactionParams().do();
        params.fee = algosdk.ALGORAND_MIN_TX_FEE;
        params.flatFee = true;

        // Build required app args as Uint8Array
        let campaignArg = new TextEncoder().encode(campaign)
        let countArg = algosdk.encodeUint64(shares);
        let appArgs = [campaignArg, countArg]

        // Create ApplicationCallTxn
        let appCallTxn = algosdk.makeApplicationCallTxnFromObject({
            from: this.getAlgoAddress(),
            appIndex: appIndex, //productid
            onComplete: algosdk.OnApplicationComplete.NoOpOC,
            suggestedParams: params,
            appArgs: appArgs
        })

        // Create PaymentTxn
        let paymentTxn = algosdk.makePaymentTxnWithSuggestedParamsFromObject({
            from: this.getAlgoAddress(),
            to: owner,
            amount: shares * price,
            suggestedParams: params
        })

        let txnArray = [appCallTxn, paymentTxn]

        // Create group transaction out of previously build transactions
        let groupID = algosdk.computeGroupID(txnArray)
        for (let i = 0; i < 2; i++) txnArray[i].group = groupID;

        // Sign & submit the group transaction
        let signedTxn = await myAlgoConnect.signTransaction(txnArray.map(txn => txn.toByte()));
        console.log("Signed group transaction");
        let tx = await this.client.sendRawTransaction(signedTxn.map(txn => txn.blob)).do();

        // Wait for group transaction to be confirmed
        let confirmedTxn = await algosdk.waitForConfirmation(this.client, tx.txId, 4);

        // Notify about completion
        console.log("Group transaction " + tx.txId + " confirmed in round " + confirmedTxn["confirmed-round"]);

    }


    // GET PRODUCTS: Use indexer
    getCampainsAction = async () => {
        console.log("Fetching campaigns...")
        let note = new TextEncoder().encode("");
        let encodedNote = Buffer.from(note).toString("base64");

        // Step 1: Get all transactions by notePrefix (+ minRound filter for performance)
        let transactionInfo = await this.indexerClient.searchForTransactions()
            .notePrefix(encodedNote)
            .txType("appl")
            .minRound(minRound)
            .do();
        let campaigns = []
        for (const transaction of transactionInfo.transactions) {
            let appId = transaction["created-application-index"]
            if (appId) {
                // Step 2: Get each application by application id
                let campaign = await getApplication(appId)
                if (campaign) {
                    campaigns.push(campaign)
                }
            }
        }
        console.log("campaigns fetched.")
        return campaigns
    }




    getApplication = async (appId) => {
        try {
            // 1. Get application by appId
            let response = await this.indexerClient.lookupApplications(appId).includeAll(true).do();
            if (response.application.deleted) {
                return null;
            }
            let globalState = response.application.params["global-state"]

            // 2. Parse fields of response and return product
            let owner = response.application.params.creator
            let name = ""
            let url = ""
            let shareAccount = ""
            let price = 0
            let totalShare = 0

            const getField = (fieldName, globalState) => {
                return globalState.find(state => {
                    return state.key === utf8ToBase64String(fieldName);
                })
            }

            if (getField("name", globalState) !== undefined) {
                let field = getField("name", globalState).value.bytes
                name = base64ToUTF8String(field)
            }

            if (getField("url", globalState) !== undefined) {
                let field = getField("url", globalState).value.bytes
                url = base64ToUTF8String(field)
            }

            if (getField("share_account", globalState) !== undefined) {
                let field = getField("share_account", globalState).value.bytes
                shareAccount = base64ToUTF8String(field)
            }

            if (getField("price_per_share", globalState) !== undefined) {
                price = getField("price_per_share", globalState).value.uint
            }

            if (getField("total_shares", globalState) !== undefined) {
                totalShare = getField("total_shares", globalState).value.uint
            }

            return { name, url, shareAccount, pricePerShare, totalShare, appId, owner }
        } catch (err) {
            return null;
        }
    }



}

export default new AlgoManager()
