
import db from "./firebase"
import { doc, onSnapshot, collection, query, where } from "firebase/firestore";
import AlgoManager from "./AlgoManager";


/*
Progetto - campaign
{
    id: 1,
    image: image1,
    category: "Design",
    date: "20 Days Left",
    title: "A New Super Car on Your Wrist",
    goal: "3600.00",
    raised: 23,
  }

*/

export const days_between = (date1) => {
    const dateNow = new Date()
    // The number of milliseconds in one day
    const ONE_DAY = 1000 * 60 * 60 * 24;
    // Calculate the difference in milliseconds
    const differenceMs = Math.abs(date1 - dateNow);
    // Convert back to days and return
    return Math.round(differenceMs / ONE_DAY) < 0 ? 0 : Math.round(differenceMs / ONE_DAY);
}

export const normalizeData = (it) => {
    return {
        id: it.data().id ?? "0",
        title: it.data().title ?? "-",
        image: it.data().image ?? "-",
        category: it.data().category ?? "Web3",
        date: it.data().date ? new Date(it.data().date) : new Date(),
        goal: it.data().goal ?? "0",
        raised: it.data().raised ?? 0,
        totalSupply: it.data().totalSupply ?? 0,
        sharePrice: it.data().sharePrice ?? 1,
        description: it.data().description ?? "",
        secondaryDescription: it.data().secondaryDescription ?? "",
        medias: it.data().medias ?? [],
    }
}


export const getAllCampaigns = () => {
    
    return new Promise((resolve, reject) => {
        // decentralized
        // const campaigns = AlgoManager.getCampainsAction()
        // resolve(campaigns)

        // centralized
        let COLLECTION_CAMPAIGN = "Campaigns"
        const q = query(collection(db, COLLECTION_CAMPAIGN))
        const unsub = onSnapshot(q, (querySnapshot) => {
            const datas = querySnapshot.docs.map(it => {
                console.log(it.data())
                return normalizeData(it)
            })
            resolve(datas)
        });
    })
}


export const getCampaignById = (id) => {
    return new Promise((resolve, reject) => {
        let COLLECTION_CAMPAIGN = "Campaigns"
        const q = query(collection(db, COLLECTION_CAMPAIGN), where("id", "==", id))
        const unsub = onSnapshot(q, (querySnapshot) => {
            const datas = querySnapshot.docs.map(it => {
                console.log(it.data())
                return normalizeData(it)
            })
            console.log("Datas", datas)
            resolve(datas)
        });
    })
}

