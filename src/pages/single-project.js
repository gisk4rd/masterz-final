import Header from "@/components/Header/Header";
import Layout from "@/components/Layout/Layout";
import ProjectDetailsArea from "@/components/ProjectsArea/ProjectDetails/ProjectDetailsArea";
import ProjectDetailsContent from "@/components/ProjectsArea/ProjectDetails/ProjectDetailsContent";
import SimilarProjects from "@/components/ProjectsArea/SimilarProjects";
import PageTitle from "@/components/Reuseable/PageTitle";
import { getCampaignById } from "@/data/db/DBManager";
import React, { useEffect, useState } from "react";

const SingleProject = () => {
  const [campaign, setCampaign] = useState(null)


  // @khadim
  // Carica la campagna da firebase e la salva in state.campaign
  const loadData = async () => {
    const params = new URLSearchParams(window.location.search);
    const prog = params.get("prog")
    const campaign = await getCampaignById(prog)
    setCampaign(campaign[0])
  }


  useEffect(() => {
    loadData()
  }, [])

  return (
    <Layout>
      <Header />
      <PageTitle title={campaign?.title} page="Explore" />
      <ProjectDetailsArea campaign={campaign} />
      <ProjectDetailsContent campaign={campaign} />
      {/*<SimilarProjects />*/}
    </Layout>
  );
};

export default SingleProject;
