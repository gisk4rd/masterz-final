import algosdk from "algosdk"

// get accounts from mnemonic

const creatorMnemonic = "CREATOR MNEMONIC"
const userMnemonic = "USER MNEMONIC"
const creatorAccount = algosdk.mnemonicToSecretKey(creatorMnemonic)
const userAccout = algosdk.mnemonicToSecretKey(userMnemonic)
const creatorSecret = creatorAccount.sk
const creatorAddress = creatorAccount.addr
const sender = userAccout.addr

//Generate Account
const account = algosdk.generateAccount()
const secrekey = account.sk
const mnemonic = algosdk.secretKeyToMnemonic(secrekey)
console.log("mnemonic " + mnemonic)
console.log("address " + account.addr)