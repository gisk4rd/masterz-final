from ast import Return
from pyteal import *

def algogo_contract():
    #Operations

    buy = Bytes("buy")
    create_campaign = Bytes("create_campaign")

    #globals
    total_invested = Bytes("total_invested")
    number_of_users = Bytes("number_of_users")
    number_of_campaigns = Bytes("number_of_campaigns")

    #locals
    price_per_share = Bytes("price")
    total_shares = Bytes("total_units")
    allocated_shares = Bytes("allocated_shares")
    token_address = Bytes("token_address")


    @Subroutine(TealType.none)
    def mint_campaign(campaign_name, campaign_symbol, share_account, total_shares_, price_per_share_, url, metadata):
        Seq(
            InnerTxnBuilder.Begin(),
            InnerTxnBuilder.SetFields({
                TxnField.type_enum: TxnType.AssetConfig,
                TxnField.config_asset_total: total_shares_,
                TxnField.config_asset_decimals: Int(0),
                TxnField.config_asset_name: campaign_name,
                TxnField.config_asset_unit_name: campaign_symbol,
                TxnField.config_asset_default_frozen: Int(0),
                TxnField.config_asset_manager: Global.current_application_address(),
                TxnField.config_asset_url: url,
                TxnField.note: metadata,
                TxnField.fee: Global.min_txn_fee()
            }
            ),
            InnerTxnBuilder.Submit(),
            App.localPut(Txn.sender(), token_address, InnerTxn.created_asset_id()),
            App.localPut(Txn.sender(), total_shares, total_shares_),
            App.localPut(Txn.sender(), price_per_share, price_per_share_),
            App.localPut(Txn.sender(), allocated_shares, Int(0)),
        )


    @Subroutine(TealType.none)
    def purchase_shares(campaign_account, number_of_shares):
        Seq(
            Assert(
                And(
                    Add(App.localGet(campaign_account, allocated_shares), number_of_shares)  <= App.localGet(campaign_account, total_shares),
                    Gtxn[1].amount() >= Mul(App.localGet(campaign_account, price_per_share), number_of_shares),
                    Gtxn[1].receiver() == campaign_account
                )
            ),

            InnerTxnBuilder.Begin(),
            InnerTxnBuilder.SetFields({
                TxnField.type_enum: TxnType.AssetTransfer,
                TxnField.xfer_asset: App.localGet(campaign_account, token_address),
                TxnField.asset_amount: number_of_shares,
                TxnField.asset_sender: Global.current_application_address(),
                TxnField.asset_receiver: Gtxn[1].sender()
            }),
            InnerTxnBuilder.Submit(),
            App.localPut(campaign_account, allocated_shares, Add(App.localGet(campaign_account, allocated_shares), number_of_shares)),
            App.globalPut(total_invested, Add(App.globalGet(total_invested), Gtxn[1].amount()))
        )

    basic_checks = And(
        Txn.rekey_to() == Global.zero_address(),
        Txn.close_remainder_to() == Global.zero_address(),
        Txn.asset_close_to() == Global.zero_address(),
    )
        
    on_deploy = Seq(
        [
            Assert(
                And(
                    Global.group_size() == Int(1),
                    basic_checks
                )
            ),
            App.globalPut(total_invested, Int(0)),
            App.globalPut(number_of_campaigns, Int(0)),
            App.globalPut(number_of_users, Int(0)),

            Approve()
        ]
    )

    handle_optin = Seq(
        [
            Assert(
                And(
                    Global.group_size() == Int(1),
                    basic_checks
                )
            ),
            App.localPut(Txn.sender(), token_address, Int(0)),
            App.localPut(Txn.sender(), total_shares, Int(0)),
            App.localPut(Txn.sender(), price_per_share, Int(0)),
            App.localPut(Txn.sender(), allocated_shares, Int(0)),
            Approve()

        ]
    )

    handle_buy = Seq(
        [
            Assert(
                And(
                    Global.group_size() == Int(2),
                    basic_checks
                )
            ),

            purchase_shares(Gtxn[0].application_args[1], Gtxn[0].application_args[2]),
            Approve()

        ]
    )

    handle_create_campaign = Seq(
        [
            Assert(
                And(
                    Global.group_size() == Int(1),
                    basic_checks
                )
            ),
            mint_campaign(
                Txn.application_args[1], Txn.application_args[2],
                Txn.sender(), Txn.application_args[3], Txn.application_args[4],
                Txn.application_args[5], Txn.application_args[6]
            ),

            Approve()
        ]
    )

        
    program = Cond(
        [
            Txn.application_id() == Int(0), on_deploy
        ],
        [
            Txn.on_completion() == OnComplete.OptIn, handle_optin
        ],
        [
            Txn.application_args[0] == buy, handle_buy
        ],
        [
            Txn.application_args[0] == create_campaign, handle_create_campaign
        ]
    )

    
    return program

if __name__ == "__main__":
    print(compileTeal(algogo_contract(), Mode.Application, version = 5))




